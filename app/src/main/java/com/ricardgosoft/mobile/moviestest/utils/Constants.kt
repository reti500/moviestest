package com.ricardgosoft.mobile.moviestest.utils

object Constants {
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val POSTER_URL = "https://image.tmdb.org/t/p/w342"
    const val BACKDROP_URL = "https://image.tmdb.org/t/p/w500"
    const val API_KEY = "54818948a9b447783cd262129dec39ba"
    const val API_LANG = "EN"

    enum class ItemCategory(val value: String) {
        Popular("popular"),
        TopRated("topRated"),
    }
}