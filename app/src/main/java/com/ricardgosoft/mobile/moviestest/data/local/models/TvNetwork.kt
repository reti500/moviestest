package com.ricardgosoft.mobile.moviestest.data.local.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class TvNetwork(

    @SerializedName("name")
    @ColumnInfo(name = "network_name")
    val name: String,

    @SerializedName("id")
    @ColumnInfo(name = "network_id")
    val id: Int,

    @SerializedName("logo_path")
    @ColumnInfo(name = "network_logo_path")
    val logoPath: String,

    @SerializedName("origin_country")
    @ColumnInfo(name = "network_origin_country")
    val originCountry: String
)
