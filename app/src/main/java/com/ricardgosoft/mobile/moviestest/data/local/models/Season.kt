package com.ricardgosoft.mobile.moviestest.data.local.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Season(
    @SerializedName("air_date")
    @ColumnInfo(name = "season_air_date")
    val airDate: String,

    @SerializedName("episode_count")
    @ColumnInfo(name = "season_episode_count")
    val episodeCount: Int,

    @SerializedName("id")
    @ColumnInfo(name = "season_id")
    val id: Int,

    @SerializedName("name")
    @ColumnInfo(name = "season_name")
    val name: String,

    @SerializedName("overview")
    @ColumnInfo(name = "season_overview")
    val overview: String,

    @SerializedName("poster_path")
    @ColumnInfo(name = "season_poster_path")
    val posterPath: String,

    @SerializedName("season_number")
    @ColumnInfo(name = "season_season_number")
    val seasonNumber: Int
)