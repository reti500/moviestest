package com.ricardgosoft.mobile.moviestest.data.api.services

import com.ricardgosoft.mobile.moviestest.data.api.responses.MovieVideosResponse
import com.ricardgosoft.mobile.moviestest.data.api.responses.MoviesResponse
import com.ricardgosoft.mobile.moviestest.data.local.entities.MovieDetail
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    @GET("movie/popular")
    suspend fun getPopularMovies(
        @Query("page") page: Int
    ): Response<MoviesResponse>

    @GET("movie/top_rated")
    suspend fun getTopRatedMovies(
        @Query("page") page: Int
    ): Response<MoviesResponse>

    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(
        @Path("movie_id") movie_id: Int
    ): Response<MovieDetail>

    @GET("movie/{movie_id}/videos")
    suspend fun getMovieVideos(
        @Path("movie_id") movie_id: Int,
    ): Response<MovieVideosResponse>

}