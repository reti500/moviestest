package com.ricardgosoft.mobile.moviestest.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ricardgosoft.mobile.moviestest.data.local.dao.*
import com.ricardgosoft.mobile.moviestest.data.local.entities.*

@Database(entities = [
    Movie::class,
    MovieDetail::class,
    MovieVideo::class,
    TVShow::class,
    TvShowDetail::class,
    SearchResult::class], version = 1, exportSchema = false)
@TypeConverters(MoviesTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun moviesDao(): MoviesDao
    abstract fun movieDetailDao(): MovieDetailDao
    abstract fun movieVideosDao(): MovieVideoDao
    abstract fun tvShowsDao(): TVShowDao
    abstract fun tvShowDetailDao(): TvShowDetailDao
    abstract fun searchDao(): SearchDao

    companion object {
        @Volatile private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance ?: synchronized(this) { instance ?: buildDatabase(context).also { instance = it}}

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, "movies_db")
                .fallbackToDestructiveMigration()
                .build()
    }
}