package com.ricardgosoft.mobile.moviestest.data.local.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Company(
    @SerializedName("id")
    @ColumnInfo(name = "company_id")
    val id: Int,

    @SerializedName("logo_path")
    @ColumnInfo(name = "company_logo_path")
    val logoPath: String?,

    @SerializedName("name")
    @ColumnInfo(name = "company_name")
    val name: String,

    @SerializedName("origin_country")
    @ColumnInfo(name = "company_origin_country")
    val originalCountry: String
)
