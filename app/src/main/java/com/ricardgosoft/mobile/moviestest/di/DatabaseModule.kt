package com.ricardgosoft.mobile.moviestest.di

import android.content.Context
import com.ricardgosoft.mobile.moviestest.data.local.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideMoviesDao(db: AppDatabase) = db.moviesDao()

    @Singleton
    @Provides
    fun provideMovieDetailDao(db: AppDatabase) = db.movieDetailDao()

    @Singleton
    @Provides
    fun provideTvShowsDao(db: AppDatabase) = db.tvShowsDao()

    @Singleton
    @Provides
    fun provideTvShowDetailDao(db: AppDatabase) = db.tvShowDetailDao()

    @Singleton
    @Provides
    fun provideMovieVideosDao(db: AppDatabase) = db.movieVideosDao()

    @Singleton
    @Provides
    fun provideSearchDao(db: AppDatabase) = db.searchDao()
}