package com.ricardgosoft.mobile.moviestest.data.api.responses

import com.google.gson.annotations.SerializedName
import com.ricardgosoft.mobile.moviestest.data.local.entities.Movie

data class MoviesResponse(
    @SerializedName("page")
    val page: Int,

    @SerializedName("results")
    val results: List<Movie>
)
