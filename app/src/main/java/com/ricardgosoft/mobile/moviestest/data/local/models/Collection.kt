package com.ricardgosoft.mobile.moviestest.data.local.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Collection(
    @SerializedName("id")
    @ColumnInfo(name = "collection_id")
    val id: Int?,

    @SerializedName("name")
    @ColumnInfo(name = "collection_name")
    val name: String,

    @SerializedName("poster_path")
    @ColumnInfo(name = "collection_poster_path")
    val posterPath: String?,

    @SerializedName("backdrop_path")
    @ColumnInfo(name = "collection_backdrop_path")
    val backdropPath: String?
)