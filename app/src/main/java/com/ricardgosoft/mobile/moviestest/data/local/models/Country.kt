package com.ricardgosoft.mobile.moviestest.data.local.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Country(
    @SerializedName("iso_3166_1")
    @ColumnInfo(name = "country_iso_3166_1")
    val iso: String,

    @SerializedName("name")
    @ColumnInfo(name = "country_name")
    val name: String
)