package com.ricardgosoft.mobile.moviestest.data.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movie_videos")
data class MovieVideo(

    @SerializedName("movie_id")
    @ColumnInfo(name = "video_movie_id")
    val movieId: Int?,

    @SerializedName("id")
    @ColumnInfo(name = "video_id")
    @PrimaryKey
    val id: String,

    @SerializedName("iso_639_1")
    @ColumnInfo(name = "video_iso_639_1")
    val iso639: String,

    @SerializedName("iso_3166_1")
    @ColumnInfo(name = "video_iso_3166_1")
    val iso3166: String,

    @SerializedName("key")
    @ColumnInfo(name = "video_key")
    val key: String,

    @SerializedName("name")
    @ColumnInfo(name = "video_name")
    val name: String,

    @SerializedName("site")
    @ColumnInfo(name = "video_site")
    val site: String,

    @SerializedName("size")
    @ColumnInfo(name = "video_size")
    val size: Int,

    @SerializedName("type")
    @ColumnInfo(name = "video_type")
    val type: String
)