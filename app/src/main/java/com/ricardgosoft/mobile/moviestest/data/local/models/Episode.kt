package com.ricardgosoft.mobile.moviestest.data.local.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Episode(

    @SerializedName("air_date")
    @ColumnInfo(name = "episode_air_date")
    val airDate: String,

    @SerializedName("episode_number")
    @ColumnInfo(name = "episode_episode_number")
    val episodeNumber: Int,

    @SerializedName("id")
    @ColumnInfo(name = "episode_id")
    val id: Int,

    @SerializedName("name")
    @ColumnInfo(name = "episode_name")
    val name: String,

    @SerializedName("overview")
    @ColumnInfo(name = "episode_overview")
    val overview: String,

    @SerializedName("production_code")
    @ColumnInfo(name = "episode_production_code")
    val productionCode: String,

    @SerializedName("season_number")
    @ColumnInfo(name = "episode_season_number")
    val seasonNumber: Int,

    @SerializedName("still_path")
    @ColumnInfo(name = "episode_still_path")
    val stillPath: String,

    @SerializedName("vote_average")
    @ColumnInfo(name = "episode_vote_average")
    val voteAverage: Float,

    @SerializedName("vote_count")
    @ColumnInfo(name = "episode_vote_count")
    val voteCount: Int
)