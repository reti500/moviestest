package com.ricardgosoft.mobile.moviestest.ui.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.ricardgosoft.mobile.moviestest.utils.Constants
import com.squareup.picasso.Picasso

@BindingAdapter("loadImage")
fun loadImage(view: ImageView, url: String) {
    Picasso.get().load(Constants.POSTER_URL + url).into(view)
}