package com.ricardgosoft.mobile.moviestest.data.api.services

import com.ricardgosoft.mobile.moviestest.data.api.responses.MovieVideosResponse
import com.ricardgosoft.mobile.moviestest.data.api.responses.TVResponse
import com.ricardgosoft.mobile.moviestest.data.local.entities.TvShowDetail
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TVService {

    @GET("tv/popular")
    suspend fun getPopularTV(
        @Query("page") page: Int
    ): Response<TVResponse>

    @GET("tv/top_rated")
    suspend fun getTopRatedTV(
        @Query("page") page: Int
    ): Response<TVResponse>

    @GET("tv/{tv_id}")
    suspend fun getTVDetail(
        @Path("tv_id") tv_id: Int,
    ): Response<TvShowDetail>

    @GET("tv/{tv_id}/videos")
    suspend fun getTvVideos(
        @Path("tv_id") tv_id: Int,
    ): Response<MovieVideosResponse>

}