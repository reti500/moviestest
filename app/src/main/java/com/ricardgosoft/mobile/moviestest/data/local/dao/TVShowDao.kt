package com.ricardgosoft.mobile.moviestest.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ricardgosoft.mobile.moviestest.data.local.entities.TVShow

@Dao
interface TVShowDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(shows: List<TVShow>)

    @Query("SELECT * FROM tv_shows WHERE category = :category")
    fun getAll(category: String): LiveData<List<TVShow>>
}