package com.ricardgosoft.mobile.moviestest.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ricardgosoft.mobile.moviestest.data.local.entities.SearchResult

@Dao
interface SearchDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(results: List<SearchResult>)

    @Query("SELECT * FROM search_results WHERE name LIKE :query")
    fun getLike(query: String): LiveData<List<SearchResult>>
}