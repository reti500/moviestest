package com.ricardgosoft.mobile.moviestest.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ricardgosoft.mobile.moviestest.data.local.entities.Movie
import com.ricardgosoft.mobile.moviestest.databinding.RowMovieItemBinding
import javax.inject.Inject

class MovieItemAdapter @Inject constructor(
) : RecyclerView.Adapter<MovieItemAdapter.ViewHolder>() {
    private var items: List<Movie> = listOf()
    private var onItemClick: (movie: Movie) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            RowMovieItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    fun setMovies(movies: List<Movie>) {
        items = movies
        notifyDataSetChanged()
    }

    fun setItemClick(click: (movie: Movie) -> Unit) {
        onItemClick = click
    }

    inner class ViewHolder constructor(
        private val binding: RowMovieItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(movieModel: Movie) {
            binding.apply {
                movie = movieModel

                rootContainer.setOnClickListener { onItemClick(movieModel) }
            }

            binding.executePendingBindings()
        }
    }
}