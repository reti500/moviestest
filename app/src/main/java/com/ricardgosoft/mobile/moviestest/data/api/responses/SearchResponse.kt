package com.ricardgosoft.mobile.moviestest.data.api.responses

import com.google.gson.annotations.SerializedName
import com.ricardgosoft.mobile.moviestest.data.local.entities.SearchResult

data class SearchResponse(
    @SerializedName("page")
    val page: Int,

    @SerializedName("results")
    val results: List<SearchResult>
)
