package com.ricardgosoft.mobile.moviestest.data.api.services

import com.ricardgosoft.mobile.moviestest.data.api.responses.SearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchService {

    @GET("search/multi")
    suspend fun searchAll(
        @Query("query") query: String,
        @Query("page") page: Int,
    ): Response<SearchResponse>

}