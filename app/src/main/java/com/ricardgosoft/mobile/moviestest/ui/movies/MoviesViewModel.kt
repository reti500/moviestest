package com.ricardgosoft.mobile.moviestest.ui.movies

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ricardgosoft.mobile.moviestest.data.local.entities.Movie
import com.ricardgosoft.mobile.moviestest.data.repository.MoviesRepository
import com.ricardgosoft.mobile.moviestest.utils.Constants
import com.ricardgosoft.mobile.moviestest.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(
    private val moviesRepository: MoviesRepository
) : ViewModel() {

    private val _moviesList = MutableLiveData<Resource<List<Movie>>>()
    var moviesList: LiveData<Resource<List<Movie>>> = _moviesList

    private var currentPage: Int = 1
    private var category: Constants.ItemCategory = Constants.ItemCategory.Popular

    // Methods
    fun setPopular() {
        changeCategory(Constants.ItemCategory.Popular)
    }

    fun setTopRated() {
        changeCategory(Constants.ItemCategory.TopRated)
    }

    fun loadMovies() {
        viewModelScope.launch {
            val result = when(category) {
                Constants.ItemCategory.Popular ->
                    moviesRepository.getPopularMovies(currentPage)
                Constants.ItemCategory.TopRated ->
                    moviesRepository.getTopRatedMovies(currentPage)
            }

            result.observeForever { _moviesList.value = it }
        }
    }

    // Private methods
    private fun changeCategory(cat: Constants.ItemCategory) {
        currentPage = 1
        category = cat
        _moviesList.value = Resource.success(listOf())
        loadMovies()
    }
}