package com.ricardgosoft.mobile.moviestest.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ricardgosoft.mobile.moviestest.data.local.entities.MovieVideo
import com.ricardgosoft.mobile.moviestest.databinding.RowMovieItemBinding
import com.ricardgosoft.mobile.moviestest.databinding.RowVideoBinding
import javax.inject.Inject

class VideoAdapter @Inject constructor(
) : RecyclerView.Adapter<VideoAdapter.ViewHolder>() {
    private var items: List<MovieVideo> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            RowVideoBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    fun setVideos(videos: List<MovieVideo>) {
        items = videos
        notifyDataSetChanged()
    }

    inner class ViewHolder constructor(
        private val binding: RowVideoBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(video: MovieVideo) {
            binding.videoPlayerTitle.text = video.name
        }
    }
}