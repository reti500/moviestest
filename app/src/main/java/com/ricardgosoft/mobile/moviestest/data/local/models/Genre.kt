package com.ricardgosoft.mobile.moviestest.data.local.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Genre(
    @SerializedName("id")
    @ColumnInfo(name = "genre_id")
    val id: Int,

    @SerializedName("name")
    @ColumnInfo(name = "genre_name")
    val name: String
)