package com.ricardgosoft.mobile.moviestest.ui.moviesdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ricardgosoft.mobile.moviestest.data.local.entities.MovieDetail
import com.ricardgosoft.mobile.moviestest.data.local.entities.MovieVideo
import com.ricardgosoft.mobile.moviestest.data.repository.MoviesRepository
import com.ricardgosoft.mobile.moviestest.utils.Status
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesDetailViewModel @Inject constructor(
    private val moviesRepository: MoviesRepository
) : ViewModel() {

    private val _movieDetail = MutableLiveData<MovieDetail>()
    val movieDetail: LiveData<MovieDetail> = _movieDetail

    private val _movieVideos = MutableLiveData<List<MovieVideo>>()
    val movieVideos: LiveData<List<MovieVideo>> = _movieVideos

    fun loadMovieDetail(movieId: Int) {
        viewModelScope.launch {
            moviesRepository.getMovieDetail(movieId).observeForever {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            it.data?.let { data -> _movieDetail.postValue(data) }
                        }
                        Status.ERROR -> {
                        }
                        Status.LOADING -> {
                        }
                    }
                }
            }
        }
    }

    fun loadMovieVideos(movieId: Int) {
        viewModelScope.launch {
            moviesRepository.getVideos(movieId).observeForever {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            it.data?.let { data -> _movieVideos.postValue(data) }
                        }
                        Status.ERROR -> {
                        }
                        Status.LOADING -> {
                        }
                    }
                }
            }
        }
    }
}