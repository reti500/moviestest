package com.ricardgosoft.mobile.moviestest.data.api.responses

import com.google.gson.annotations.SerializedName
import com.ricardgosoft.mobile.moviestest.data.local.entities.TVShow

data class TVResponse(
    @SerializedName("page")
    val page: Int,

    @SerializedName("results")
    val results: List<TVShow>
)
