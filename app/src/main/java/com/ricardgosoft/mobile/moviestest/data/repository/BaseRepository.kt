package com.ricardgosoft.mobile.moviestest.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.ricardgosoft.mobile.moviestest.utils.Resource
import com.ricardgosoft.mobile.moviestest.utils.Status
import kotlinx.coroutines.Dispatchers

abstract class BaseRepository {
    protected fun <T, A> get(
        databaseQuery: () -> LiveData<T>,
        networkCall: suspend () -> Resource<A>,
        saveCallResult: suspend (A) -> Unit
    ): LiveData<Resource<T>> = liveData(Dispatchers.IO) {
        // Start Loading
        emit(Resource.loading(null))

        // Find data on local database and emit source
        val source = databaseQuery.invoke().map { Resource.success(it) }
        emitSource(source)

        // Find data on API
        val resp = networkCall.invoke()
        if (resp.status == Status.SUCCESS) {
            saveCallResult(resp.data!!)
        } else if (resp.status == Status.ERROR) {
            emit(Resource.error(null, resp.message!!))
            emitSource(source)
        }
    }
}