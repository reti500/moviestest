package com.ricardgosoft.mobile.moviestest.ui.moviesdetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.ricardgosoft.mobile.moviestest.R
import com.ricardgosoft.mobile.moviestest.data.local.entities.MovieDetail
import com.ricardgosoft.mobile.moviestest.databinding.FragmentMoviesDetailBinding
import com.ricardgosoft.mobile.moviestest.ui.adapters.VideoAdapter
import com.ricardgosoft.mobile.moviestest.utils.Constants
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MoviesDetailFragment : Fragment(R.layout.fragment_movies_detail) {

    private val viewModel: MoviesDetailViewModel by viewModels()
    private val args: MoviesDetailFragmentArgs by navArgs()
    private lateinit var binding: FragmentMoviesDetailBinding

    @Inject
    lateinit var adapter: VideoAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentMoviesDetailBinding.bind(view)

        setupObservers()
    }

    private fun setupObservers() {
        viewModel.loadMovieDetail(args.movieId.toInt())

        viewModel.movieDetail.observe(viewLifecycleOwner) {
            bindView(it)
        }

        viewModel.movieVideos.observe(viewLifecycleOwner) {
            adapter.setVideos(it)
        }
    }

    private fun bindView(details: MovieDetail) {
        val path = details.backdropPath ?: details.posterPath
        Picasso.get().load(Constants.BACKDROP_URL + path).into(binding.movieDetailImage)
        binding.movieDetailTitle.text = details.title
        binding.movieDetailDescription.text = details.overview
        binding.movieDetailVideos.layoutManager = LinearLayoutManager(requireContext())
        binding.movieDetailVideos.adapter = adapter

        viewModel.loadMovieVideos(args.movieId.toInt())
    }
}