package com.ricardgosoft.mobile.moviestest.data.local.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.ricardgosoft.mobile.moviestest.data.local.models.*

@Entity(tableName = "tv_show_detail")
data class TvShowDetail(
    @SerializedName("id")
    @PrimaryKey
    val id: Int,

    @SerializedName("backdrop_path")
    val backdropPath: String?,

    @SerializedName("created_by")
    val createdBy: List<Creator>,

    @SerializedName("episode_run_time")
    val episodeRunTime: List<Int>,

    @SerializedName("first_air_date")
    val firstAirDate: String,

    @SerializedName("genres")
    val genres: List<Genre>,

    @SerializedName("homepage")
    val homePage: String,

    @SerializedName("in_production")
    val inProduction: Boolean,

    @SerializedName("languages")
    val languages: List<String>,

    @SerializedName("last_air_date")
    val lastAirDate: String,

    @SerializedName("last_episode_to_air")
    @Embedded
    val lastEpisodeToAir: Episode,

    @SerializedName("name")
    val name: String,

    @SerializedName("networks")
    val networks: List<TvNetwork>,

    @SerializedName("number_of_episodes")
    val numberOfEpisodes: Int,

    @SerializedName("number_of_seasons")
    val numberOfSeasons: Int,

    @SerializedName("origin_country")
    val originCountry: List<String>,

    @SerializedName("original_language")
    val originalLanguage: String,

    @SerializedName("original_name")
    val originalName: String,

    @SerializedName("overview")
    val overview: String,

    @SerializedName("popularity")
    val popularity: Float,

    @SerializedName("poster_path")
    val posterPath: String,

    @SerializedName("production_companies")
    val productionCompanies: List<Company>,

    @SerializedName("production_countries")
    val productionCountries: List<Country>,

    @SerializedName("seasons")
    val seasons: List<Season>,

    @SerializedName("spoken_languages")
    val spokenLanguages: List<SpokenLanguage>,

    @SerializedName("status")
    val status: String,

    @SerializedName("tagline")
    val tagLine: String,

    @SerializedName("type")
    val type: String,

    @SerializedName("vote_average")
    val voteAverage: Float,

    @SerializedName("vote_count")
    val voteCount: Int
)