package com.ricardgosoft.mobile.moviestest.data.remote

import com.ricardgosoft.mobile.moviestest.data.api.services.MovieService
import javax.inject.Inject

class MoviesRemoteDataSource @Inject constructor(
    private val moviesService: MovieService
): BaseDataSource() {
    suspend fun getPopularMovies(page: Int) = getResult { moviesService.getPopularMovies(page) }
    suspend fun getTopRatedMovies(page: Int) = getResult { moviesService.getTopRatedMovies(page) }
    suspend fun getMovieDetail(movieId: Int) = getResult { moviesService.getMovieDetail(movieId) }
    suspend fun getMovieVideos(movieId: Int) = getResult { moviesService.getMovieVideos(movieId) }
}