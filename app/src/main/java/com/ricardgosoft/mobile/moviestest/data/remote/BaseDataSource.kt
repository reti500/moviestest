package com.ricardgosoft.mobile.moviestest.data.remote

import android.util.Log
import com.ricardgosoft.mobile.moviestest.utils.Resource
import retrofit2.Response
import java.lang.Exception

abstract class BaseDataSource {

    protected suspend fun <T> getResult(call: suspend () -> Response<T>): Resource<T> {
        try {
            Log.e("Service", "remote")
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return Resource.success(body)
            }

            return error(" ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            Log.e("Service", "Ex: $e")
            return error(e.message ?: e.toString())
        }
    }

    private fun <T> error(message: String): Resource<T> {
        Log.e("remoteDataSource", message)
        return Resource.error(null, "Network call has failed for a following reason: $message")
    }
}