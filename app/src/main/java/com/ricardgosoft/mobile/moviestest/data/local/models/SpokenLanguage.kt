package com.ricardgosoft.mobile.moviestest.data.local.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class SpokenLanguage(
    @SerializedName("english_name")
    @ColumnInfo(name = "spoken_english_name")
    val englishName: String,

    @SerializedName("iso_639_1")
    @ColumnInfo(name = "spoken_iso_639_1")
    val iso: String,

    @SerializedName("name")
    @ColumnInfo(name = "spoken_name")
    val name: String
)