package com.ricardgosoft.mobile.moviestest.di

import com.ricardgosoft.mobile.moviestest.data.local.dao.MovieDetailDao
import com.ricardgosoft.mobile.moviestest.data.local.dao.MovieVideoDao
import com.ricardgosoft.mobile.moviestest.data.local.dao.MoviesDao
import com.ricardgosoft.mobile.moviestest.data.remote.MoviesRemoteDataSource
import com.ricardgosoft.mobile.moviestest.data.repository.MoviesRepository
import com.ricardgosoft.mobile.moviestest.ui.adapters.MovieItemAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MoviesModule {

    @Singleton
    @Provides
    fun providesDayItemAdapter(): MovieItemAdapter = MovieItemAdapter()

    @Singleton
    @Provides
    fun provideMoviesRepository(
        remoteDataSource: MoviesRemoteDataSource,
        localMoviesDataSource: MoviesDao,
        localMovieDetailDataSource: MovieDetailDao,
        localMovieVideos: MovieVideoDao
    ) = MoviesRepository(
            remoteDataSource,
            localMoviesDataSource,
            localMovieDetailDataSource,
            localMovieVideos
        )
}