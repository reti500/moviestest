package com.ricardgosoft.mobile.moviestest.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ricardgosoft.mobile.moviestest.data.local.entities.MovieVideo

@Dao
interface MovieVideoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(videos: List<MovieVideo>)

    @Query("SELECT * FROM movie_videos WHERE video_movie_id = :movieId")
    fun getAll(movieId: Int): LiveData<List<MovieVideo>>
}