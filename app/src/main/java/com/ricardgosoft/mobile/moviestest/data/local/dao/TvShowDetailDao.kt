package com.ricardgosoft.mobile.moviestest.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ricardgosoft.mobile.moviestest.data.local.entities.TvShowDetail

@Dao
interface TvShowDetailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOne(mDetail: TvShowDetail)

    @Query("SELECT * FROM tv_show_detail WHERE id = :showId")
    fun get(showId: Int): LiveData<TvShowDetail>
}