package com.ricardgosoft.mobile.moviestest.data.local.models

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Creator(
    @SerializedName("id")
    @ColumnInfo(name = "creator_id")
    val id: Int,

    @SerializedName("credit_id")
    @ColumnInfo(name = "creator_credit_id")
    val creditId: String,

    @SerializedName("name")
    @ColumnInfo(name = "creator_name")
    val name: String,

    @SerializedName("gender")
    @ColumnInfo(name = "creator_gender")
    val gender: Int,

    @SerializedName("profile_path")
    @ColumnInfo(name = "creator_profile_path")
    val profilePath: String?
)