package com.ricardgosoft.mobile.moviestest.data.repository

import com.ricardgosoft.mobile.moviestest.data.local.dao.MovieDetailDao
import com.ricardgosoft.mobile.moviestest.data.local.dao.MovieVideoDao
import com.ricardgosoft.mobile.moviestest.data.local.dao.MoviesDao
import com.ricardgosoft.mobile.moviestest.data.remote.MoviesRemoteDataSource
import com.ricardgosoft.mobile.moviestest.utils.Constants
import javax.inject.Inject

class MoviesRepository @Inject constructor(
    private val remoteDataSource: MoviesRemoteDataSource,
    private val localMoviesDataSource: MoviesDao,
    private val localMoviesDetailDatasource: MovieDetailDao,
    private val localMovieVideoDataSource: MovieVideoDao,
) : BaseRepository() {
    fun getPopularMovies(page: Int) = get(
        databaseQuery = { localMoviesDataSource.getAll(Constants.ItemCategory.Popular.value) },
        networkCall = { remoteDataSource.getPopularMovies(page) },
        saveCallResult = { localMoviesDataSource.insertAll(it.results.map { item ->
            item.copy(category = Constants.ItemCategory.Popular.value) }) }
    )

    fun getTopRatedMovies(page: Int) = get(
        databaseQuery = { localMoviesDataSource.getAll(Constants.ItemCategory.TopRated.value) },
        networkCall = { remoteDataSource.getTopRatedMovies(page) },
        saveCallResult = { localMoviesDataSource.insertAll(it.results.map { item ->
            item.copy(category = Constants.ItemCategory.TopRated.value) }) }
    )

    fun getMovieDetail(movieId: Int) = get(
        databaseQuery = { localMoviesDetailDatasource.get(movieId) },
        networkCall = { remoteDataSource.getMovieDetail(movieId) },
        saveCallResult = { localMoviesDetailDatasource.insertOne(it) }
    )

    fun getVideos(movieId: Int) = get(
        databaseQuery = { localMovieVideoDataSource.getAll(movieId) },
        networkCall = { remoteDataSource.getMovieVideos(movieId) },
        saveCallResult = { localMovieVideoDataSource.insertAll(it.results.map { item ->
            item.copy(movieId = movieId)
        }) }
    )
}