package com.ricardgosoft.mobile.moviestest.data.remote

import com.ricardgosoft.mobile.moviestest.data.api.services.TVService
import javax.inject.Inject

class TvShowsRemoteDataSource @Inject constructor(
    private val tvService: TVService
): BaseDataSource() {
    suspend fun getPopularTVShows(page: Int) = getResult { tvService.getPopularTV(page) }
    suspend fun getTopRatedTVShows(page: Int) = getResult { tvService.getTopRatedTV(page) }
    suspend fun getTvShowDetail(showId: Int) = getResult { tvService.getTVDetail(showId) }
    suspend fun getTvShowVideos(showId: Int) = getResult { tvService.getTvVideos(showId) }
}