package com.ricardgosoft.mobile.moviestest.data.remote

import com.ricardgosoft.mobile.moviestest.data.api.services.SearchService
import javax.inject.Inject

class SearchRemoteDataSource @Inject constructor(
    private val searchService: SearchService
): BaseDataSource() {
    suspend fun search(query: String, page: Int) = getResult { searchService.searchAll(query, page) }
}