package com.ricardgosoft.mobile.moviestest.ui.movies

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ricardgosoft.mobile.moviestest.R
import com.ricardgosoft.mobile.moviestest.data.local.entities.Movie
import com.ricardgosoft.mobile.moviestest.databinding.FragmentMoviesBinding
import com.ricardgosoft.mobile.moviestest.ui.adapters.MovieItemAdapter
import com.ricardgosoft.mobile.moviestest.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MoviesFragment : Fragment(R.layout.fragment_movies) {

    private lateinit var binding: FragmentMoviesBinding
    private val viewModel: MoviesViewModel by viewModels()

    @Inject
    lateinit var adapter: MovieItemAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentMoviesBinding.bind(view)

        setupSpinner()
        setupRecycler()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.moviesList.observe(viewLifecycleOwner) {
            it?.let { response ->
                when (response.status) {
                    Status.SUCCESS -> {
                        Log.e("observer", "Success: $response")
                        adapter.setMovies(it.data ?: emptyList())
                        adapter.setItemClick { movie ->
                            findNavController().navigate(
                                MoviesFragmentDirections.actionMoviesFragmentToMoviesDetailFragment(movie.id.toString())
                            )
                        }
                    }
                    Status.ERROR -> { Log.e("observer", "Error -> ${response.message}")}
                    Status.LOADING -> { Log.e("observer", "Loading") }
                }
            }
        }

        viewModel.loadMovies()
    }

    private fun setupSpinner() {
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.filterArray,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.orderSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    when (position) {
                        0 -> viewModel.setPopular()
                        1 -> viewModel.setTopRated()
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {}
            }
            binding.orderSpinner.adapter = adapter
        }
    }

    private fun setupRecycler() {
        binding.moviesList.layoutManager = LinearLayoutManager(requireContext())
        binding.moviesList.adapter = adapter
        binding.moviesList.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                /*if (!isLoading) {
                    layoutManager.let {
                        if (it.findLastCompletelyVisibleItemPosition() == (moviesAdapter?.itemCount ?: 0) - 1) {
                            viewModel.loadMoreMovies()
                        }
                    }
                }*/
            }
        })
    }

    private fun onSelectedMovie(movie: Movie) {
        val b = bundleOf("movieId" to movie.id)
        //findNavController().navigate(R.id.movieDetailFragment, b)
    }
}