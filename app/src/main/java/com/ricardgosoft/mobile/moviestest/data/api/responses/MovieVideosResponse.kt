package com.ricardgosoft.mobile.moviestest.data.api.responses

import com.google.gson.annotations.SerializedName
import com.ricardgosoft.mobile.moviestest.data.local.entities.MovieVideo

data class MovieVideosResponse(
    @SerializedName("id")
    val id: Int,

    @SerializedName("results")
    val results: List<MovieVideo>
)
