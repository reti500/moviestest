package com.ricardgosoft.mobile.moviestest.data.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ricardgosoft.mobile.moviestest.data.local.models.*
import java.lang.reflect.Type

class MoviesTypeConverters {
    // For List of int
    @TypeConverter
    fun fromIntegerList(ids: List<Int>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<Int>>() {}.type
        return gson.toJson(ids, type)
    }

    @TypeConverter
    fun toIntegerList(idsString: String): List<Int> {
        val gson = Gson()
        val type = object : TypeToken<List<Int>>() {}.type
        return gson.fromJson(idsString, type)
    }

    // For List of String
    @TypeConverter
    fun fromStringList(list: List<String>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<String>>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toStringList(str: String): List<String> {
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(str, type)
    }

    // For list of Genre
    @TypeConverter
    fun fromGenresList(genres: List<Genre>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<Genre>>() {}.type
        return gson.toJson(genres, type)
    }

    @TypeConverter
    fun toGenresList(genresStr: String): List<Genre> {
        val gson = Gson()
        val type = object : TypeToken<List<Genre>>() {}.type
        return gson.fromJson(genresStr, type)
    }

    // For list of Company
    @TypeConverter
    fun fromCompaniesList(companies: List<Company>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<Company>>() {}.type
        return gson.toJson(companies, type)
    }

    @TypeConverter
    fun toCompaniesList(companiesStr: String): List<Company> {
        val gson = Gson()
        val type = object : TypeToken<List<Company>>() {}.type
        return gson.fromJson(companiesStr, type)
    }

    // For list of Country
    @TypeConverter
    fun fromCountryList(list: List<Country>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<Country>>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toCountryList(str: String): List<Country> {
        val gson = Gson()
        val type = object : TypeToken<List<Country>>() {}.type
        return gson.fromJson(str, type)
    }

    // For list of Spoken
    @TypeConverter
    fun fromSpokenLanguageList(list: List<SpokenLanguage>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<SpokenLanguage>>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toSpokenLanguageList(str: String): List<SpokenLanguage> {
        val gson = Gson()
        val type = object : TypeToken<List<SpokenLanguage>>() {}.type
        return gson.fromJson(str, type)
    }

    // For list of Creator
    @TypeConverter
    fun fromCreatorLanguageList(list: List<Creator>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<Creator>>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toCreatorList(str: String): List<Creator> {
        val gson = Gson()
        val type = object : TypeToken<List<Creator>>() {}.type
        return gson.fromJson(str, type)
    }

    // For list of Network
    @TypeConverter
    fun fromNetworkLanguageList(list: List<TvNetwork>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<TvNetwork>>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toNetworkList(str: String): List<TvNetwork> {
        val gson = Gson()
        val type = object : TypeToken<List<TvNetwork>>() {}.type
        return gson.fromJson(str, type)
    }

    // For list of Season
    @TypeConverter
    fun fromSeasonLanguageList(list: List<Season>): String {
        val gson = Gson()
        val type: Type = object : TypeToken<List<Season>>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toSeasonList(str: String): List<Season> {
        val gson = Gson()
        val type = object : TypeToken<List<Season>>() {}.type
        return gson.fromJson(str, type)
    }
}